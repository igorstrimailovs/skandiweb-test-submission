/*
    Product list page
 */
class Product{
    constructor() {
        this._products = [];
    }

    addProduct(arr) {
        this._products.push(arr);
    }

    generate() {
        this._products.forEach(function (value) {
            $('#products').append(`
                <div data-id="${value.id}" class="position-relative product-item border rounded shadow px-3 py-2 mt-4 mx-3 text-center">${value.sku}<br>${value.name}<br>${value.price} $<br>${value.attr}</div>
            `);
        });

        $('.product-item').each(function () {
            $(this).append(`<input type="checkbox" style="position: absolute; top:10px;left:10px;">`)
        });
    }
}

$('#delete').click(function () {
    let todel = [];
    $('input[type="checkbox"]:checked').each(function () {
        todel.push($(this).parent().data('id'));
    });
    if (todel.length != 0) {
        $.ajax({
            url: "/product/delete",
            data: {todel: todel},
            type: "post"
        }).done(function () {
            $('.product-item').each(function () {
                if (todel.includes($(this).data('id'))) {
                    $(this).remove();
                }
            })
        });
    }
});

/*
    Product add page
 */
$(document).ready(function () {
    $('#type').val(1);
});
$('#createProduct select').change(function () {
    switch (parseInt($(this).val())) {
        case 1:
            $('.attrOptions').html(`
                <input id="size"required class="form-control" type="number" step="0.001" placeholder="Enter size">`);
            break;
        case 2:
            $('.attrOptions').html(`
                <input id="weight" required class="form-control" type="number"step="0.001" placeholder="Enter weight">`);
            break;
        case 3:
            $('.attrOptions').html(`
                <input id="height" required class="form-control" type="number" step="1" placeholder="Enter Height">
                <input id="width" required class="form-control" type="number" step="1" placeholder="Enter Width">
                <input id="length" required class="form-control" type="number" step="1" placeholder="Enter Length">`);
            break;
    }
});

function validate(){
    $.ajax({
        url: "/product/create",
        type: "post",
        data: {
            sku: $('#sku').val(),
            type: $('#type').val(),
            name: $('#name').val(),
            price: $('#price').val(),
            size: $('#size').val(),
            weight: $('#weight').val(),
            height: $('#height').val(),
            width: $('#width').val(),
            length: $('#length').val(),
        }
    }).always(function (data) {
        let answer = $.parseJSON(data);
        if (answer.status == 'OK') {
            window.location.href = "/product/list";
        }  else {
            $('.error').html(answer.status);
        }
    });
    return false;
}




