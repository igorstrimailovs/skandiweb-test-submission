<?php

class product extends Controller
{
    //render view
    public function list() {
        $products = (new ProductModel)->getProducts();
        $this->render('products', $products);
    }

    //create new product
    public function create() {
        switch ($_POST['type']) {
            case 1:
                $product = new ProductType1Model();
                $product->setMB($_POST['size']);
                $product->setAttr($product->getAttr());
                break;
            case 2:
                $product = new ProductType2Model();
                $product->setKG($_POST['weight']);
                $product->setAttr($product->getAttr());
                break;
            case 3:
                $product = new ProductType3Model();
                $product->setHeight($_POST['height']);
                $product->setWidth($_POST['width']);
                $product->setLength($_POST['length']);
                $product->setAttr($product->getAttr());
                break;
        }
        $product->setName($_POST['name']);
        $product->setType($_POST['type']);
        $product->setPrice($_POST['price']);
        $product->setSKU($_POST['sku']);
        echo json_encode($product->save());
        die();
    }

    //delete product or products
    public function delete(){
        $IDs = $_POST['todel'];
        var_dump($IDs);
        foreach ($IDs as  $ID) {
            (new ProductModel)->delete($ID);
        }
    }

    //Render add page
    public function add() {
        $this->render('add_product');
    }

}