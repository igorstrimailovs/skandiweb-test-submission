<?php


abstract class ProductAbstractModel {

    protected $name;
    protected $type;
    protected $price;
    protected $sku;
    protected $attr;
    protected static $con;

    public function __construct() {

        self::$con = (new Model())::getDB();
    }
    
    abstract public function getAttr();

    public function getName() {
        return $this->name;
    }
    public function getType() {
        return $this->type;
    }
    public function getPrice() {
        return $this->price;
    }
    public function getSKU() {
        return $this->sku;
    }
    public function setName($newName) {
        $this->name = $newName;
    }
    public function setType($newType) {
        $this->type = $newType;
    }
    public function setPrice($newPrice) {
        $this->price = $newPrice;
    }
    public function setSKU($newSKU) {
        $this->sku = $newSKU;
    }
    public function setAttr($newAttr) {
        $this->attr = $newAttr;
    }
}