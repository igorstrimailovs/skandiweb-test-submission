<?php

class ProductType2Model extends productmodel {

    private $kg;

    public function setKG($_kg) {
        $this->kg = $_kg;
    }

    public  function getKG() {
        return $this->kg;
    }

    public function getAttr() {
        return 'Weight: '.$this->kg.' KG';
    }

}