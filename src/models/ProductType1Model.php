<?php

class ProductType1Model extends productmodel {

    private $mb;

    public function setMB($_mb) {
        $this->mb = $_mb;
    }

    public  function getMB() {
        return $this->mb;
    }

    public function getAttr() {
        return 'Size: '.$this->mb.' MB';
    }

}