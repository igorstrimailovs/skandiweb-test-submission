<?php

class ProductType3Model extends productmodel {

    private $height;
    private $width;
    private $length;

    public function setHeight($_height) {
        $this->height = $_height;
    }
    public function getHeight() {
        return $this->height;
    }
    public function setWidth($_width) {
        $this->width = $_width;
    }
    public function getWidth($_width) {
        return $this->width;
    }
    public function setLength($_length){
        $this->length = $_length;
    }
    public function getLength(){
        return $this->length;
    }


    public function getAttr() {
        return 'Dimension: '.$this->height.'x'.$this->width.'x'.$this->length;
    }

}