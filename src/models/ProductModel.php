<?php

class ProductModel extends ProductAbstractModel {

    public function getAttr(){}

    public static function delete($id)
    {
        $del = self::$con->prepare("DELETE FROM products WHERE id=?");
        $del->execute([$id]);
    }

    public static function getProducts()
    {
        return self::$con->query('SELECT * FROM products')->fetchAll();
    }

    public function save() {
        $select = 'SELECT SKU FROM products WHERE SKU=?';
        $check = self::$con->prepare($select);
        $check->execute([$this->getSKU()]);
        if ($check->rowCount() > 0) {
            return ['status' => 'Please provide a unique SKU. Thank you.'];
        } else {
            $insert = 'INSERT INTO products (name, price, type, attr, sku) VALUES (?,?,?,?,?)';
            $check = self::$con->prepare($insert);
            $check->execute([$this->getName(), $this->getPrice(), $this->getType(), $this->getAttr(), $this->getSKU()]);

            if ($check) {
                return ['status' => 'OK'];
            } else {
                return ['status' => $check->errorCode()];
            }
        }
    }

}