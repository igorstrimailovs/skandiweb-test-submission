<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="../public/css/site.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <!-- Header -->
    <div class="row mt-2">
        <div class="col-5">
            <span class="h1">Add product</span>
        </div>
        <div class="col-7 flex-row-reverse d-flex">
            <div class="nav my-auto">
                <a href="/product/list">Product list</a>
                <a href="/product/add">Add product</a>
            </div>
        </div>

    </div>
    <hr>
    <!-- Body -->
    <form id="createProduct" type="post" onsubmit="return validate()" class="text-center mx-auto">
        <input id="sku" required class="form-control" type="text" max="65" placeholder="Enter SKU">
        <input id="name" required class="form-control" type="text" max="65" placeholder="Enter Name">
        <input id="price" required class="form-control" type="number" step="0.01" min="0.01" placeholder="Enter price">
        <select id="type" required class="custom-select">
            <option value="1">Size (in MB)</option>
            <option value="2">Weight (in KG)</option>
            <option value="3">Dimensions (HxWxL)</option>
        </select>
        <div class="attrOptions">
            <input id="size"required class="form-control" step="0.001" type="number" placeholder="Enter size">
        </div>
        <button type="submit" class=" w-50 btn btn-primary">Create</button>
        <div class="error mt-3 text-danger text-center"></div>
    </form>
</div>
</body>
</html>
<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
<script src="../public/js/site.js"></script>