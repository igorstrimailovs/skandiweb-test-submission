<html>
<head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="../public/css/site.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <!-- Header -->
    <div class="row mt-2">
        <div class="col-3">
            <span class="h1">Product list</span>
        </div>
        <div class="col-4 d-flex">
            <div class="nav my-auto">
                <a href="/product/list">Product list</a>
                <a href="/product/add">Add product</a>
            </div>
        </div>
        <div class="col-5 d-flex">
            <div class="actions my-auto ml-auto">
                <button id="delete" class="btn btn-secondary">Mass delete</button>
            </div>
        </div>
    </div>
    <hr>

    <!-- Body -->
    <div id="products" class="d-flex justify-content-center flex-wrap w-100">
    </div>
</div>
</body>
</html>

<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
<script src="../public/js/site.js"></script>
<script>
    let products = new Product();

    //Filling products
    <?php
    foreach ($data as $product) { ?>
        products.addProduct(<?=json_encode($product)?>);
    <?php }
    ?>

    //Init
    products.generate();
</script>